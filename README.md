# elaw-charts

## Helm Charts

<code>Lint</code>
```
helm lint . --debug
```

<code>Check templates</code>
```
helm template elaw-app-charts . --debug
```

<code>Install chart</code>
```
helm install elaw-app-charts --namespace elaw8 --create-namespace .
```

<code>Upgrade chart</code>
```
helm upgrade elaw-app-charts --namespace elaw8 .
```

<code>Update chart dependencies</code>
```
helm dependency build
helm dependency update # - after first build
```

<code>Generate secret for Private Registry</code>
```
kubectl create secret docker-registry --dry-run=client image-repo-auth \
    --docker-server=registry.elaw.com.br \
    --docker-username=elaw8cicd \
    --docker-password=******* \
    --docker-email=elaw8cicd@elaw.com.br \
    --namespace=elaw8 \
    -o yaml > image-repo-auth.yaml
```