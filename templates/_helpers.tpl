{{/*
    Default Global Config Map
*/}}
{{- define "global.config.contents" -}}
java.tool.opts: {{ required ".Values.global.configmap.data.java_tool_opts is required" .Values.global.configmap.data.java_tool_opts | quote }}
kafka.bootstrap.servers: {{ required ".Values.global.configmap.kafka_bootstrap_servers is required" .Values.global.configmap.data.kafka_bootstrap_servers | quote }}
keycloak.base.url: {{ required ".Values.global.configmap.keycloak_base_url is required" .Values.global.configmap.data.keycloak_base_url | quote }}
keycloak.realm: {{ required ".Values.global.configmap.keycloak_realm is required" (include "app.namespace" .) | quote }}
zipkin.endpoint.url: {{ required ".Values.global.configmap.zipkin_endpoint_url is required" .Values.global.configmap.data.zipkin_endpoint_url | quote }}
{{- end }}

{{/*
    Default Global Secret
*/}}
{{- define "global.secret.contents" -}}
keycloak.admin.user: {{ required ".Values.global.secret.data.keycloak_admin_user" .Values.global.secret.data.keycloak_admin_user | b64enc }}
keycloak.admin.pass: {{ required ".Values.global.secret.data.keycloak_admin_pass" .Values.global.secret.data.keycloak_admin_pass | b64enc }}
mongo.user: {{ required ".Values.global.secret.data.mongo_user" .Values.global.secret.data.mongo_user | b64enc }}
mongo.pass: {{ required ".Values.global.secret.data.mongo_pass" .Values.global.secret.data.mongo_pass | b64enc }}
openapi.oauth.client_id: {{ required ".Values.global.secret.data.openapi_oauth_client_id" .Values.global.secret.data.openapi_oauth_client_id | b64enc }}
openapi.oauth.client_secret: {{ required ".Values.global.secret.data.openapi_oauth_client_secret" .Values.global.secret.data.openapi_oauth_client_secret | b64enc }}
{{- end }}

{{/*
    Defaults
*/}}
{{- define "app.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end }}

{{- define "app.namespace" -}}
{{/*{{- default .Values.global.environment.namespace .Values.namespaceOverride | trunc 63 | trimSuffix "-" -}}*/}}
{{- default .Release.Namespace .Values.namespace | trunc 63 | trimSuffix "-" -}}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "app.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "app.labels" -}}
helm.sh/chart: {{ include "app.chart" . }}
{{ include "app.selectorLabels" . }}
{{- if .Chart.AppVersion }}
{{ include "app.versionLabels" . }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "app.selectorLabels" -}}
app.kubernetes.io/name: {{ include "app.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "app.versionLabels" -}}
app.kubernetes.io/version: {{ .Values.image.tag | default .Chart.AppVersion }}
{{- end }}